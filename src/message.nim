## Message format and processing 

import json
import strutils
import options

type
  MessageKind* = enum
    Talk,
    Join,
    Leave,
    Info,

  Message* = ref object
    case kind*: MessageKind
    of Talk:
      sender*: string
      content*: string
    of Join, Leave:
      who*: string
    of Info:
      participants*: seq[string]
    timestamp*: int64

proc `$`*(message: Message): string =
  ## Convert message to json object
  var jsonObject = %* { "kind": ($message.kind).toLowerAscii(),
                        "timestamp": message.timestamp }
  case message.kind
  of Talk:
    jsonObject["sender"] = % message.sender
    jsonObject["content"] = % message.content
  of Join, Leave:
    jsonObject["who"] = % message.who
  of Info:
    jsonObject["participants"] = % message.participants
  $jsonObject

proc parse*(input: string): Option[Message] =
  try:
    let node = parseJson(input)
    var msg: Message
    case node["kind"].to(string)
    of "talk":
      msg = Message(kind: Talk, sender: node["sender"].to(string),
                    content: node["content"].to(string),
                    timestamp: node["timestamp"].to(int64))
    of "join":
      msg = Message(kind: Join, who: node["who"].to(string),
                    timestamp: node["timestamp"].to(int64))
    of "leave":
      msg = Message(kind: Leave, who: node["who"].to(string),
                    timestamp: node["timestamp"].to(int64))
    of "info":
      msg = Message(kind: Info,
                    participants: node["participants"].to(seq[string]),
                    timestamp: node["timestamp"].to(int64))
    else:
      raise newException(KeyError, "invalid message kind")
    some(msg)
  except JsonParsingError, KeyError, JsonKindError:
    none(Message)
